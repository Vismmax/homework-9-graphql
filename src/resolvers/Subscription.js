function newPostSubscribe(parent, args, context, info) {
  return context.prisma.$subscribe
    .post({
      mutation_in: ['CREATED'],
    })
    .node();
}

const newPost = {
  subscribe: newPostSubscribe,
  resolve: (payload) => {
    return payload;
  },
};

function newCommentSubscribe(parent, args, context, info) {
  return context.prisma.$subscribe
    .comment({
      mutation_in: ['CREATED'],
    })
    .node();
}

const newComment = {
  subscribe: newCommentSubscribe,
  resolve: (payload) => {
    return payload;
  },
};

function newLikeSubscribe(parent, args, context, info) {
  return context.prisma.$subscribe
    .like({
      mutation_in: ['CREATED', 'UPDATED', 'DELETED'],
    })
    .node();
}

const newLike = {
  subscribe: newLikeSubscribe,
  resolve: (payload) => {
    return payload;
  },
};

function newDislikeSubscribe(parent, args, context, info) {
  return context.prisma.$subscribe
    .dislike({
      mutation_in: ['CREATED', 'UPDATED', 'DELETED'],
    })
    .node();
}

const newDislike = {
  subscribe: newDislikeSubscribe,
  resolve: (payload) => {
    return payload;
  },
};

function newLikeCommentSubscribe(parent, args, context, info) {
  return context.prisma.$subscribe
    .likeComment({
      mutation_in: ['CREATED', 'UPDATED', 'DELETED'],
    })
    .node();
}

const newLikeComment = {
  subscribe: newLikeCommentSubscribe,
  resolve: (payload) => {
    return payload;
  },
};

function newDislikeCommentSubscribe(parent, args, context, info) {
  return context.prisma.$subscribe
    .dislikeComment({
      mutation_in: ['CREATED', 'UPDATED', 'DELETED'],
    })
    .node();
}

const newDislikeComment = {
  subscribe: newDislikeCommentSubscribe,
  resolve: (payload) => {
    return payload;
  },
};

module.exports = {
  newPost,
  newComment,
  newLike,
  newDislike,
  newLikeComment,
  newDislikeComment,
};
