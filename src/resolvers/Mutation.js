function createPost(parent, args, context, info) {
  console.log('args', args);
  return context.prisma.createPost({
    text: args.text,
  });
}

async function createComment(parent, { text, postId }, context, info) {
  const postExists = await context.prisma.$exists.post({
    id: postId,
  });

  if (!postExists) {
    throw new Error(`Post with ID ${postId} does not exist`);
  }

  return context.prisma.createComment({
    text,
    post: { connect: { id: postId } },
  });
}

async function createLike(parent, { like, postId }, context, info) {
  const postExists = await context.prisma.$exists.post({
    id: postId,
  });

  if (!postExists) {
    throw new Error(`Post with ID ${postId} does not exist`);
  }

  return context.prisma.createLike({
    like,
    post: { connect: { id: postId } },
  });
}

async function createDislike(parent, { dislike, postId }, context, info) {
  const postExists = await context.prisma.$exists.post({
    id: postId,
  });

  if (!postExists) {
    throw new Error(`Post with ID ${postId} does not exist`);
  }

  return context.prisma.createDislike({
    dislike,
    post: { connect: { id: postId } },
  });
}

async function createLikeComment(parent, { like, commentId }, context, info) {
  const commentExists = await context.prisma.$exists.comment({
    id: commentId,
  });

  if (!commentExists) {
    throw new Error(`Comment with ID ${commentId} does not exist`);
  }

  return context.prisma.createLikeComment({
    like,
    comment: { connect: { id: commentId } },
  });
}

async function createDislikeComment(parent, { dislike, commentId }, context, info) {
  const commentExists = await context.prisma.$exists.comment({
    id: commentId,
  });

  if (!commentExists) {
    throw new Error(`Comment with ID ${commentId} does not exist`);
  }

  return context.prisma.createDislikeComment({
    dislike,
    comment: { connect: { id: commentId } },
  });
}

module.exports = {
  createPost,
  createComment,
  createLike,
  createDislike,
  createLikeComment,
  createDislikeComment,
};
