async function post({ id }, args, context) {
  const post = await context.prisma.comment({ id }).post();

  return post;
}

async function likes({ id }, args, context) {
  const where = {
    like: true,
    comment: {
      id,
    },
  };
  const likeList = await context.prisma.comment({ id }).likes({ where });
  const count = await context.prisma.likeCommentsConnection({ where }).aggregate().count();

  return {
    likeList,
    count,
  };
}

async function dislikes({ id }, args, context) {
  const where = {
    dislike: true,
    comment: {
      id,
    },
  };
  const dislikeList = await context.prisma.comment({ id }).dislikes({ where });
  const count = await context.prisma.dislikeCommentsConnection({ where }).aggregate().count();

  return {
    dislikeList,
    count,
  };
}

module.exports = {
  post,
  likes,
  dislikes,
};
