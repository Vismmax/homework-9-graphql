async function comments({ id }, args, context) {
  const comments = await context.prisma.post({ id }).comments();

  return comments;
}

async function likes({ id }, args, context) {
  const where = {
    like: true,
    post: {
      id,
    },
  };
  const likeList = await context.prisma.post({ id }).likes({ where });
  const count = await context.prisma.likesConnection({ where }).aggregate().count();

  return {
    likeList,
    count,
  };
}

async function dislikes({ id }, args, context) {
  const where = {
    dislike: true,
    post: {
      id,
    },
  };
  const dislikeList = await context.prisma.post({ id }).dislikes({ where });
  const count = await context.prisma.dislikesConnection({ where }).aggregate().count();

  return {
    dislikeList,
    count,
  };
}

module.exports = {
  comments,
  likes,
  dislikes,
};
