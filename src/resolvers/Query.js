async function posts(parent, { filter, skip, first, orderBy }, context) {
  const where = filter
    ? {
        text: filter,
      }
    : {};

  const postList = await context.prisma.posts({
    where,
    skip,
    first,
    orderBy,
  });

  const count = context.prisma.postsConnection({ where }).aggregate().count();

  return {
    postList,
    count,
  };
}

async function comments(parent, args, context) {
  console.log('ccc');
  const comments = await context.prisma.comments();

  return comments;
}

async function likes(parent, args, context) {
  const where = { like: true };
  const likeList = await context.prisma.likes({ where });
  const count = await context.prisma.likesConnection({ where }).aggregate().count();
  return {
    likeList,
    count,
  };
}

async function dislikes(parent, args, context) {
  const where = { dislike: true };
  const dislikeList = await context.prisma.dislikes({ where });
  const count = await context.prisma.dislikesConnection({ where }).aggregate().count();
  return {
    dislikeList,
    count,
  };
}

async function likes(parent, args, context) {
  const where = { like: true };
  const likeList = await context.prisma.likes({ where });
  const count = await context.prisma.likesConnection({ where }).aggregate().count();
  return {
    likeList,
    count,
  };
}

async function dislikes(parent, args, context) {
  const where = { dislike: true };
  const dislikeList = await context.prisma.dislikes({ where });
  const count = await context.prisma.dislikesConnection({ where }).aggregate().count();
  return {
    dislikeList,
    count,
  };
}

async function post(parent, { id }, context) {
  const post = await context.prisma.post({ id });

  return post;
}

module.exports = {
  posts,
  comments,
  likes,
  dislikes,
  post,
};
