import React from 'react';
import { Menu } from 'semantic-ui-react';
import './Pagination.css';

function Pagination({ countPosts, countPostsOnPage, currentPage, setCurrentPage }) {
  const countPages = Math.ceil(countPosts / countPostsOnPage);
  const pages = [];

  const handleItemClick = (e, { name }) => setCurrentPage(parseInt(name) - 1);

  for (let i = 0; i < countPages; i++) {
    pages.push(
      <Menu.Item
        key={i}
        name={(i + 1).toString()}
        active={i === currentPage}
        onClick={handleItemClick}
      />,
    );
  }

  return (
    <div className="footer">
      <Menu pagination>{pages}</Menu>
    </div>
  );
}

export default Pagination;
