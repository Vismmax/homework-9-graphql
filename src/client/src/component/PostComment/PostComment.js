import React from 'react';
import { Comment, Icon } from 'semantic-ui-react';

function PostComment({ comment: { id, text, createdAt, likes, dislikes }, like, dislike }) {
  return (
    <Comment>
      <Comment.Content>
        <Comment.Metadata className="post-author">{createdAt}</Comment.Metadata>
        <Comment.Text>{text}</Comment.Text>
        <Comment.Actions>
          <Comment.Action>
            <Icon name="thumbs up" onClick={() => like(id)} /> {likes.count} Likes
          </Comment.Action>
          <Comment.Action>
            <Icon name="thumbs down" onClick={() => dislike(id)} /> {dislikes.count} Dislikes
          </Comment.Action>
        </Comment.Actions>
      </Comment.Content>
    </Comment>
  );
}

export default PostComment;
