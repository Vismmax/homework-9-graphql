import React, { useEffect, useState } from 'react';
import { Segment, Comment } from 'semantic-ui-react';
import './Chat.css';
import Post from '../Post/Post';
import { useMutation, useQuery, useSubscription } from '@apollo/client';
import { GET_POSTS } from '../../queries/query';
import {
  CREATE_COMMENT_MUTATION,
  CREATE_DISLIKE_MUTATION,
  CREATE_DISLIKECOMMENT_MUTATION,
  CREATE_LIKE_MUTATION,
  CREATE_LIKECOMMENT_MUTATION,
} from '../../queries/mutation';
import Pagination from '../Pagination/Pagination';
import { NEW_POST_SUBSCRIPTION } from '../../queries/subscription';

function Chat({ filter, orderBy }) {
  const countPostsOnPage = 5;
  const [currentPage, setCurrentPage] = useState(0);
  const { loading, error, data, refetch } = useQuery(GET_POSTS, {
    variables: { skip: currentPage, first: countPostsOnPage, filter, orderBy },
    pollInterval: 500,
  });
  const [addComment] = useMutation(CREATE_COMMENT_MUTATION);
  const [likePost] = useMutation(CREATE_LIKE_MUTATION);
  const [dislikePost] = useMutation(CREATE_DISLIKE_MUTATION);
  const [likeComment] = useMutation(CREATE_LIKECOMMENT_MUTATION);
  const [dislikeComment] = useMutation(CREATE_DISLIKECOMMENT_MUTATION);

  const addCommentHandler = ({ text, postId }) => {
    addComment({ variables: { text, postId } });
  };

  const likePostHandler = (postId) => {
    likePost({ variables: { like: true, postId } });
  };

  const dislikePostHandler = (postId) => {
    dislikePost({ variables: { dislike: true, postId } });
  };

  const likeCommentHandler = (commentId) => {
    likeComment({ variables: { like: true, commentId } });
  };

  const dislikeCommentHandler = (commentId) => {
    dislikeComment({ variables: { dislike: true, commentId } });
  };

  if (error) console.log(`Error! ${error.message}`);

  const posts = data?.posts?.postList.map((post) => (
    <Post
      key={post.id}
      post={post}
      addComment={addCommentHandler}
      like={likePostHandler}
      dislike={dislikePostHandler}
      likeComment={likeCommentHandler}
      dislikeComment={dislikeCommentHandler}
    />
  ));

  return (
    <>
      <Segment className="chat" loading={loading}>
        <Comment.Group>{posts}</Comment.Group>
      </Segment>
      <Pagination
        countPosts={data?.posts?.count}
        countPostsOnPage={countPostsOnPage}
        currentPage={currentPage}
        setCurrentPage={setCurrentPage}
      />
    </>
  );
}

export default Chat;
