import React, { useState } from 'react';
import { Comment, Icon, Button } from 'semantic-ui-react';
import PostComment from '../PostComment/PostComment';
import './Post.css';
import PostEditor from '../PostEditor/PostEditor';

function Post({ post, addComment, like, dislike, likeComment, dislikeComment }) {
  const [showEditor, setShowEditor] = useState(false);
  const { id, text, createdAt, comments, likes, dislikes } = post;

  const addCommentHandler = (text) => {
    addComment({ text, postId: id });
    setShowEditor(false);
  };

  const commentElements = comments.map((comment) => (
    <PostComment key={comment.id} comment={comment} like={likeComment} dislike={dislikeComment} />
  ));

  return (
    <Comment>
      <Comment.Content>
        <Comment.Metadata className="post-author">{createdAt}</Comment.Metadata>
        <Comment.Text>{text}</Comment.Text>
        <Comment.Actions className="post-actions">
          <Comment.Action>
            <Icon name="thumbs up" onClick={() => like(id)} /> {likes.count} Likes
          </Comment.Action>
          <Comment.Action>
            <Icon name="thumbs down" onClick={() => dislike(id)} /> {dislikes.count} Dislikes
          </Comment.Action>
          <Comment.Action className="action-reply">
            <Button
              size="mini"
              icon={showEditor ? 'cancel' : 'pencil'}
              content={showEditor ? 'Cancel' : 'Reply'}
              onClick={() => setShowEditor(!showEditor)}
            />
          </Comment.Action>
        </Comment.Actions>
      </Comment.Content>
      {showEditor && <PostEditor onSubmit={addCommentHandler} />}
      <Comment.Group>{commentElements}</Comment.Group>
    </Comment>
  );
}

export default Post;
