import React, { useState } from 'react';
import { Container } from 'semantic-ui-react';
import './App.css';
import Header from '../Header/Header';
import Chat from '../Chat/Chat';
import PostEditor from '../PostEditor/PostEditor';
import { useMutation } from '@apollo/client';
import { CREATE_POST_MUTATION } from '../../queries/mutation';

function App() {
  const [filter, setFilter] = useState('');
  const [orderBy, setOrderBy] = useState('createdAt_ASC');
  const [addPost] = useMutation(CREATE_POST_MUTATION);

  const addPostHandler = (text) => {
    addPost({ variables: { text } });
  };

  return (
    <Container className="app">
      <Header setFilter={setFilter} setOrderBy={setOrderBy} />
      <Chat filter={filter} orderBy={orderBy} />
      <PostEditor onSubmit={addPostHandler} />
    </Container>
  );
}

export default App;
