import React, { useState } from 'react';
import { Form, Button } from 'semantic-ui-react';
import './PostEditor.css';

function PostEditor({ onSubmit }) {
  const [text, setText] = useState('');

  const onSubmitHandler = (ev) => {
    ev.preventDefault();
    if (text) {
      onSubmit(text);
      setText('');
    }
  };

  return (
    <Form className="post-editor">
      <div className="textarea-wrap">
        <textarea rows={2} value={text} onChange={(e) => setText(e.target.value)} />
      </div>
      <div className="send-wrap">
        <Button
          size="huge"
          type="submit"
          icon="paper plane"
          content="Send"
          disabled={!text}
          onClick={onSubmitHandler}
        />
      </div>
    </Form>
  );
}

export default PostEditor;
