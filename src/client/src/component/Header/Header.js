import React, { useState } from 'react';
import { Menu, Form } from 'semantic-ui-react';

function Header({ setFilter, setOrderBy }) {
  const [value, setValue] = useState('');
  const onSubmitHandler = () => {
    setFilter(value);
    setValue('');
  };

  const sort = (order) => {
    setOrderBy(order);
  };
  return (
    <Menu>
      <Menu.Item header>GraphQL Chat</Menu.Item>
      <Menu.Menu position="right">
        <Menu.Item>
          <Form onSubmit={onSubmitHandler}>
            <input
              placeholder="Search..."
              value={value}
              onChange={(e) => setValue(e.target.value)}
            />
          </Form>
        </Menu.Item>
        <Menu.Item icon="sort amount down" onClick={() => sort('createdAt_ASC')} />
        <Menu.Item icon="sort amount up" onClick={() => sort('createdAt_DESC')} />
      </Menu.Menu>
    </Menu>
  );
}

export default Header;
