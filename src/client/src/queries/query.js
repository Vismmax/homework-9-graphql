import { gql } from '@apollo/client';

export const GET_POSTS = gql`
  query getPosts($orderBy: PostOrderByInput, $filter: String, $skip: Int, $first: Int) {
    posts(orderBy: $orderBy, filter: $filter, skip: $skip, first: $first) {
      count
      postList {
        id
        text
        createdAt
        comments {
          id
          text
          likes {
            count
          }
          dislikes {
            count
          }
        }
        likes {
          count
        }
        dislikes {
          count
        }
      }
    }
  }
`;
