import { gql } from '@apollo/client';

export const NEW_POST_SUBSCRIPTION = gql`
  subscription NewPost {
    newPost {
      id
      text
      createdAt
      comments {
        id
        text
        createdAt
      }
      likes {
        count
      }
      dislikes {
        count
      }
    }
  }
`;

export const NEW_COMMENT_SUBSCRIPTION = gql`
  subscription NewComment {
    newComment {
      id
      text
      createdAt
      post {
        id
      }
    }
  }
`;

export const NEW_LIKE_SUBSCRIPTION = gql`
  subscription NewLike {
    newLike {
      id
      like
      createdAt
      post {
        id
      }
    }
  }
`;

export const NEW_DISLIKE_SUBSCRIPTION = gql`
  subscription NewDislike {
    newDislike {
      id
      dislike
      createdAt
      post {
        id
      }
    }
  }
`;

export const NEW_LIKE_COMMENT_SUBSCRIPTION = gql`
  subscription NewLikeComment {
    newLikeComment {
      id
      like
      createdAt
      comment {
        id
      }
    }
  }
`;

export const NEW_DISLIKE_COMMENT_SUBSCRIPTION = gql`
  subscription NewDislikeComment {
    newDislikeComment {
      id
      dislike
      createdAt
      comment {
        id
      }
    }
  }
`;
