import { gql } from '@apollo/client';

export const CREATE_POST_MUTATION = gql`
  mutation AddPost($text: String!) {
    createPost(text: $text) {
      id
      text
      createdAt
      comments {
        id
        text
        createdAt
      }
      likes {
        count
      }
      dislikes {
        count
      }
    }
  }
`;

export const CREATE_COMMENT_MUTATION = gql`
  #  todo: $postId: ID!
  mutation AddComment($text: String!, $postId: String!) {
    createComment(text: $text, postId: $postId) {
      id
      text
      createdAt
    }
  }
`;

export const CREATE_LIKE_MUTATION = gql`
  #  todo: $postId: ID!
  mutation AddLike($like: Boolean!, $postId: String!) {
    createLike(like: $like, postId: $postId) {
      id
      like
      createdAt
    }
  }
`;

export const CREATE_DISLIKE_MUTATION = gql`
  #  todo: $postId: ID!
  mutation AddDislike($dislike: Boolean!, $postId: String!) {
    createDislike(dislike: $dislike, postId: $postId) {
      id
      dislike
      createdAt
    }
  }
`;

export const CREATE_LIKECOMMENT_MUTATION = gql`
  #  todo: $postId: ID!
  mutation AddLikeComment($like: Boolean!, $commentId: String!) {
    createLikeComment(like: $like, commentId: $commentId) {
      id
      like
      createdAt
    }
  }
`;

export const CREATE_DISLIKECOMMENT_MUTATION = gql`
  #  todo: $postId: ID!
  mutation AddDislikeComment($dislike: Boolean!, $commentId: String!) {
    createDislikeComment(dislike: $dislike, commentId: $commentId) {
      id
      dislike
      createdAt
    }
  }
`;
